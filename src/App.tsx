import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import Login from './components/Pages/Login';
import MainPage from './components/Pages/MainPage';
import useToken from './hooks/useToken';

function App() {
  const { token, setToken } = useToken();

  if (!token) {
    return <Login setToken={setToken} />;
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/main" element={<MainPage />} />
        <Route path="/" element={<Navigate replace to="/main" />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
