import { combineReducers, configureStore } from '@reduxjs/toolkit';
import contactsStore from './reducers/ContactSlice';

const rootReducer = combineReducers({
  contactsStore
});

export const createStore = () => {
  return configureStore({
    reducer: rootReducer
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof createStore>;
export type AppDispatch = AppStore['dispatch'];
