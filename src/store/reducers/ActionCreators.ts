import { RootState, AppDispatch } from '../store';
import axios from 'axios';
import { contactSlice } from './ContactSlice';

export const fetchContacts =
  (value: string) => async (dispatch: AppDispatch) => {
    try {
      dispatch(contactSlice.actions.contactsFetching());
      const response = await axios.get<IContact[]>(
        `http://localhost:4000/contacts${value ? `?q=${value}` : ''}`
      );
      dispatch(contactSlice.actions.contactsFetchingSuccess(response.data));
    } catch (e) {
      if (e instanceof Error) {
        dispatch(contactSlice.actions.contactsFetchingError(e.message));
      }
    }
  };

export const deleteContact =
  (contact: IContact) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const { contacts } = getState().contactsStore;
      await axios.delete<IContact[]>(
        `http://localhost:4000/contacts/${contact.id}`
      );
      dispatch(
        contactSlice.actions.setContacts(
          contacts.filter((i) => i.id !== contact.id)
        )
      );
    } catch (e) {
      if (e instanceof Error) {
        dispatch(contactSlice.actions.contactsFetchingError(e.message));
      }
    }
  };

export const updateContact =
  (contact: IContact) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const { contacts } = getState().contactsStore;
      await axios.put<IContact[]>(
        `http://localhost:4000/contacts/${contact.id}`,
        contact
      );
      dispatch(
        contactSlice.actions.setContacts(
          contacts.map((i) => (i.id === contact.id ? contact : i))
        )
      );
    } catch (e) {
      if (e instanceof Error) {
        dispatch(contactSlice.actions.contactsFetchingError(e.message));
      }
    }
  };

export const createContact =
  (contact: IContact) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      console.log('res1');
      const { contacts } = getState().contactsStore;
      const res = await axios.post<IContact>(
        `http://localhost:4000/contacts`,
        contact
      );
      dispatch(contactSlice.actions.setContacts([...contacts, res.data]));
    } catch (e) {
      if (e instanceof Error) {
        dispatch(contactSlice.actions.contactsFetchingError(e.message));
      }
    }
  };
