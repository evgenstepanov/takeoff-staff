import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ContactState {
  contacts: IContact[];
  isLoading: boolean;
  error: string;
}

const initialState: ContactState = {
  contacts: [],
  isLoading: false,
  error: ''
};

export const contactSlice = createSlice({
  name: 'contacts',
  initialState,
  reducers: {
    contactsFetching(state) {
      state.isLoading = true;
    },
    contactsFetchingSuccess(state, action: PayloadAction<IContact[]>) {
      state.isLoading = false;
      state.contacts = action.payload;
    },
    contactsFetchingError(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    setContacts(state, action: PayloadAction<IContact[]>) {
      state.contacts = action.payload;
    }
  }
});

export const {
  contactsFetching,
  contactsFetchingSuccess,
  contactsFetchingError,
  setContacts
} = contactSlice.actions;
export default contactSlice.reducer;
