import React from 'react';
import ContactContainer from '../Contacts/ContactContainer';
import CreationContainer from '../Contacts/CreationContainer';
import SearchContainer from '../Contacts/SearchContainer';

export default function MainPage() {
  return (
    <div className="main-page">
      <SearchContainer />
      <CreationContainer />
      <ContactContainer />
    </div>
  );
}
