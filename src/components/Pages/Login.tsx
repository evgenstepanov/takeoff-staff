import axios from 'axios';
import React, { useState } from 'react';

async function loginUser(body: ILogin) {
  const res = await axios.post<ILoginRes>('http://localhost:4000/login', body);
  return res.data.accessToken;
}

interface IArg {
  setToken: (token: string) => void;
}

export default function Login({ setToken }: IArg) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    const token = await loginUser({
      email,
      password
    });
    setToken(token);
  };

  return (
    <div className="login-container">
      <div className="login-wrapper">
        <h1>Please Log In</h1>
        <form className="login-form" onSubmit={handleSubmit}>
          <label>
            <p>Username</p>
            <input
              className="input"
              type="email"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setEmail(e.target.value)
              }
              value={email}
            />
          </label>
          <label>
            <p>Password</p>
            <input
              className="input"
              type="password"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setPassword(e.target.value)
              }
              value={password}
            />
          </label>
          <div>
            <button className="button login-submit" type="submit">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
