import React, { useEffect, useMemo } from 'react';
import ReactDOM from 'react-dom';

interface IArg {
  children: React.ReactNode;
}

export default function Portal({ children }: IArg) {
  const container = useMemo(() => document.createElement('div'), []);

  useEffect(() => {
    document.body.appendChild(container);
    console.log('создал');
    return () => {
      document.body.removeChild(container);
      console.log('удалил');
    };
  }, [container]);

  return ReactDOM.createPortal(children, container);
}
