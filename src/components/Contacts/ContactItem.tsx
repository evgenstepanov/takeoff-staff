import React, { useState } from 'react';
import ModalWindowCreateOrUpdate from './ModalWindowCreateOrUpdate';

interface IArg {
  contact: IContact;
  remove: (contact: IContact) => void;
}

export default function ContactItem({ contact, remove }: IArg) {
  const [isOpened, setIsOpened] = useState(false);

  return (
    <li className="contact-item">
      <div className="contact-data">
        {contact.name}, {contact.cellphone}, {contact.email}
      </div>
      <button className="button contact-delete" onClick={() => remove(contact)}>
        Delete
      </button>
      <button className="button contact-edit" onClick={() => setIsOpened(true)}>
        Edit
      </button>
      {isOpened && (
        <ModalWindowCreateOrUpdate
          handleClose={() => setIsOpened(false)}
          editMode
          data={contact}
        />
      )}
    </li>
  );
}
