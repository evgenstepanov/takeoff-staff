import React, { useEffect, useState } from 'react';
import { useTypedDispatch } from '../../hooks/redux';
import { fetchContacts } from '../../store/reducers/ActionCreators';

export default function SearchContainer() {
  const [value, setValue] = useState('');

  const handleSearch = async (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const dispatch = useTypedDispatch();

  useEffect(() => {
    dispatch(fetchContacts(value));
  }, [value, dispatch]);

  return (
    <div className="search-block">
      <label className="search-block-label">
        <p className="search-block-text">Search</p>
        <input
          className="input"
          type="email"
          onChange={handleSearch}
          value={value}
        />
      </label>
    </div>
  );
}
