import React from 'react';
import { useTypedDispatch, useTypedSelector } from '../../hooks/redux';
import { deleteContact } from '../../store/reducers/ActionCreators';
import ContactItem from './ContactItem';

const ContactContainer = () => {
  const dispatch = useTypedDispatch();
  const { contacts, isLoading, error } = useTypedSelector(
    (state) => state.contactsStore
  );

  const handleRemove = (contact: IContact) => {
    dispatch(deleteContact(contact));
  };

  return (
    <div className="contact-list">
      {isLoading && <h1 className="message">Loading...</h1>}
      {error && <h1 className="message">Error</h1>}
      <ul>
        {contacts && !isLoading && !error
          ? contacts.map((contact) => (
              <ContactItem
                remove={handleRemove}
                key={contact.id}
                contact={contact}
              />
            ))
          : null}
      </ul>
    </div>
  );
};

export default ContactContainer;
