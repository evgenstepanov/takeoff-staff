import React, { useRef, useState } from 'react';
import { useTypedDispatch } from '../../hooks/redux';
import useOnClickOutside from '../../hooks/useOnClickOutside';
import {
  createContact,
  updateContact
} from '../../store/reducers/ActionCreators';
import Portal from '../Portal/Portal';

interface IArg {
  editMode?: boolean;
  handleClose: () => void;
  data?: IContact;
}

export default function ModalWindowCreateOrUpdate({
  editMode,
  handleClose,
  data
}: IArg) {
  const ref = useRef<HTMLFormElement>(null);

  const dispatch = useTypedDispatch();

  const handleClickOutside = () => {
    handleClose();
  };

  const [email, setEmail] = useState(data ? data.email : '');
  const [cellphone, setCellPhone] = useState(data ? data.cellphone : '');
  const [name, setName] = useState(data ? data.name : '');

  useOnClickOutside(ref, handleClickOutside);

  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    const body = { ...data, email, cellphone, name };
    if (editMode && data) {
      dispatch(updateContact(body));
    } else dispatch(createContact(body));
    handleClose();
  };

  return (
    <Portal>
      <div className="modal-window">
        <form className="modal-form" ref={ref} onSubmit={handleSubmit}>
          {editMode ? <h2>Editing</h2> : <h2>Create new one</h2>}
          <label>
            <p>Name</p>
            <input
              className="input"
              type="text"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setName(e.target.value)
              }
              value={name}
              key="1"
            />
          </label>
          <label>
            <p>Email</p>
            <input
              className="input"
              type="email"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setEmail(e.target.value)
              }
              value={email}
              key="2"
            />
          </label>
          <label>
            <p>Phone</p>
            <input
              className="input"
              type="text"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setCellPhone(e.target.value)
              }
              value={cellphone}
              key="3"
            />
          </label>
          <div>
            <button type="submit">Submit</button>
          </div>
        </form>
      </div>
    </Portal>
  );
}
