import React, { useState } from 'react';
import ModalWindowCreateOrUpdate from './ModalWindowCreateOrUpdate';

export default function CreationContainer() {
  const [isOpened, setIsOpened] = useState(false);

  return (
    <div className="create-container">
      <button
        className="button"
        type="button"
        onClick={() => setIsOpened(true)}
      >
        Create new One
      </button>
      {isOpened && (
        <ModalWindowCreateOrUpdate handleClose={() => setIsOpened(false)} />
      )}
    </div>
  );
}
