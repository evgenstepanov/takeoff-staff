interface ILogin {
  email: string;
  password: string;
}

interface ILoginRes {
  accessToken: string;
}

interface IContact {
  id?: number;
  name: string;
  email: string;
  cellphone: string;
}
